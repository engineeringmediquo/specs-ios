Pod::Spec.new do |spec|
    spec.name = 'mediquo-videocall-lib'
    spec.version = '0.0.8'
    spec.summary = 'MediQuo VideoCall Framework for iOS Lib'
    spec.authors = { "Medipremium, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2020 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-videocall-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'mediquo_videocall_lib'
    spec.frameworks = ['Foundation', 'CoreLocation', 'UIKit']

    spec.static_framework = true

    spec.dependency 'Alamofire', '4.9.1'
    spec.dependency 'SDWebImage', '5.3.1'
    spec.dependency 'SwiftFormat/CLI', '0.40.14'
    spec.dependency 'SwiftGen', '6.1.0'
    spec.dependency 'SwiftLint', '0.37.0'
    spec.dependency 'OpenTok', '2.16.6'

    #   Only for development
    spec.source_files = 'mediquo-videocall-lib/**/*.{swift}'
    spec.resource_bundles = {
        'mediquo_videocall_lib' => ['mediquo-videocall-lib/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
    }
    spec.resources = ['mediquo-videocall-lib/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
end
