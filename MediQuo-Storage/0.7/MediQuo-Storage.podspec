Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Storage'
    spec.version = '0.7'
    spec.summary = 'MediQuo Storage Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    spec.source = { :http => 'https://mediquo.bintray.com/generic/Storage_0.7.zip' }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoStorage'
    # spec.preserve_paths = 'MediQuo/Storage.framework'
    # spec.source_files = 'Storage.framework/Headers/*.{h}'
    # spec.ios.public_header_files = 'Storage.framework/Headers/*.h'
    spec.ios.vendored_frameworks = 'MediQuo/Storage.framework'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit']
    # spec.ios.dependency 'Socket.IO-Client-Swift', '~> 12.1.2'
    spec.ios.dependency 'RealmSwift', '~> 3.0.1'
    spec.ios.dependency 'RxSwift', '~> 4.0.0'
end
