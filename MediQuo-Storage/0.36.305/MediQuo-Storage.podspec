Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Storage'
    spec.version = "0.36.305"
    spec.summary = 'MediQuo Storage Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2019 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.2'

    spec.module_name = 'MediQuoStorage'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Schema', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Controller', "#{spec.version}"
    spec.ios.dependency 'RealmSwift', '4.1.0'

    spec.source_files = 'Storage/Storage/Source/**/*.{swift}'
end
