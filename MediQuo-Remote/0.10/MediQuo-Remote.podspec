#!/usr/bin/ruby

Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Remote'
    spec.version = '0.10'
    spec.summary = 'MediQuo Remote Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    # spec.source = { :git => 'https://gitlab.com/mediquo/medi-ios.git', :branch => :tag => "#{spec.version}" }
    spec.source = { :http => "https://mediquo.bintray.com/generic/MediQuoRemote_0.10.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoRemote'
    spec.ios.frameworks = ['Foundation', 'UIKit']
    spec.ios.vendored_frameworks = 'MediQuo/MediQuoRemote.framework'

    spec.ios.dependency 'MediQuo-Core', "0.10"
    spec.ios.dependency 'MediQuo-Schema', "0.10"
    spec.ios.dependency 'MediQuo-Controller', "0.10"
    spec.ios.dependency 'AlamofireImage', '3.3.0'
    spec.ios.dependency 'Alamofire', '4.5.1'

    # spec.source_files = 'Remote/Remote/Source/**/*.{swift}'
end
