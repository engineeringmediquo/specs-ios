#!/usr/bin/ruby

Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Controller'
    spec.version = '0.10'
    spec.summary = 'MediQuo Controller Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    # spec.source = { :git => 'https://gitlab.com/mediquo/medi-ios.git', :branch => :tag => "#{spec.version}" }
    spec.source = { :http => "https://mediquo.bintray.com/generic/MediQuoController_0.10.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoController'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit', 'Photos']
    spec.ios.vendored_frameworks = 'MediQuo/MediQuoController.framework'

    spec.ios.dependency 'MediQuo-Core', "0.10"
    spec.ios.dependency 'MediQuo-Schema', "0.10"
    spec.ios.dependency 'RxBlocking', '4.0.0'
    spec.ios.dependency 'RxSwift', '4.0.0'
    spec.ios.dependency 'SwiftDate', '4.5'

    # spec.source_files = 'Controller/Controller/Source/**/*.{swift}'
end
