Pod::Spec.new do |spec|
    spec.name = 'mediquo-videocall-lib'
    spec.version = '2.0.0'
    spec.summary = 'MediQuo VideoCall Framework for iOS Lib'
    spec.authors = { "MediQuo, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2021 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "https://bitbucket.org/engineeringmediquo/mediquo-videocall-lib-ios", :tag => spec.version }
    spec.homepage = 'https://bitbucket.org/engineeringmediquo/mediquo-videocall-lib-ios'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.2'

    spec.module_name = 'mediquo_videocall_lib'
    spec.frameworks = ['Foundation', 'CoreLocation', 'UIKit']

    spec.static_framework = true

    spec.dependency 'Alamofire', '4.9.1'
    spec.dependency 'SwiftFormat/CLI', '0.40.14'
    spec.dependency 'SwiftGen', '6.4.0'
    spec.dependency 'SwiftLint', '0.37.0'
    spec.dependency 'OpenTok', '2.16.6'
    spec.dependency 'PromiseKit', '6.12.0'

    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    #   Only for development
    spec.source_files = 'mediquo-videocall-lib/**/*.{swift}'
    spec.resource_bundles = {
        'mediquo_videocall_lib' => ['mediquo-videocall-lib/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
    }
    spec.resources = ['mediquo-videocall-lib/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
end
