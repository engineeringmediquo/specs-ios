Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Controller'
    spec.version = "6.5.0"
    spec.summary = 'MediQuo Controller Framework for iOS'
    spec.authors = { "MediQuo, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2021 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:engineeringmediquo/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.mediquo.com'
    spec.ios.deployment_target = '13.0'
    spec.platform = :ios, '13.0'
    spec.swift_version = '5.2'

    spec.module_name = 'MediQuoController'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit', 'Photos', 'AdSupport']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Schema', "#{spec.version}"
    spec.ios.dependency 'RxBlocking', '6.0.0'
    spec.ios.dependency 'RxSwift', '6.0.0'
    spec.ios.dependency 'SwiftDate', '6.1.1'
    spec.ios.dependency 'AWSMobileClient', '2.12.1'
    spec.ios.dependency 'AWSS3', '2.12.1'

    spec.source_files = 'Controller/Controller/Source/**/*.{swift}'
end
