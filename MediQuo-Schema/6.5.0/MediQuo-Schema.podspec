Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Schema'
    spec.version = "6.5.0"
    spec.summary = 'MediQuo Schema Framework for iOS'
    spec.authors = { "MediQuo, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2021 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:engineeringmediquo/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.mediquo.com'
    spec.ios.deployment_target = '13.0'
    spec.platform = :ios, '13.0'
    spec.swift_version = '5.2'

    spec.module_name = 'MediQuoSchema'
    spec.ios.frameworks = ['Foundation', 'UIKit', 'AdSupport']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"

    spec.source_files = 'Schema/Schema/Source/**/*.{swift}'
end
