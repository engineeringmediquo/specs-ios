Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Storage'
    spec.version = '0.9.2'
    spec.summary = 'MediQuo Storage Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    spec.source = { :http => "https://mediquo.bintray.com/generic/Storage_0.9.2.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoStorage'
    spec.ios.vendored_frameworks = 'MediQuo/Storage.framework'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit']
    # spec.ios.dependency 'Socket.IO-Client-Swift', '~> 12.1.3'
    spec.ios.dependency 'RealmSwift', '~> 3.0.2'
    spec.ios.dependency 'RxSwift', '~> 4.0.0'
end
