Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Controller'
    spec.version = "0.36.1"
    spec.summary = 'MediQuo Controller Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2019 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'MediQuoController'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit', 'Photos', 'AdSupport']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Schema', "#{spec.version}"
    spec.ios.dependency 'RxBlocking', '5.0.1'
    spec.ios.dependency 'RxSwift', '5.0.1'
    spec.ios.dependency 'SwiftDate', '6.1.1'
    spec.ios.dependency 'AWSMobileClient', '2.12.1'
    spec.ios.dependency 'AWSS3', '2.12.1'

    spec.source_files = 'Controller/Controller/Source/**/*.{swift}'
end
