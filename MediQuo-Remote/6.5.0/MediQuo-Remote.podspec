Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Remote'
    spec.version = "6.5.0"
    spec.summary = 'MediQuo Remote Framework for iOS'
    spec.authors = { "MediQuo, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2021 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:engineeringmediquo/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.mediquo.com'
    spec.ios.deployment_target = '13.0'
    spec.platform = :ios, '13.0'
    spec.swift_version = '5.2'

    spec.module_name = 'MediQuoRemote'
    spec.ios.frameworks = ['Foundation', 'UIKit']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Schema', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Controller', "#{spec.version}"
    spec.ios.dependency 'AlamofireImage', '3.6.0'
    spec.ios.dependency 'Alamofire', '4.9.1'

    spec.source_files = 'Remote/Remote/Source/**/*.{swift}'
end
