Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Base'
    spec.version = '0.0.46'
    spec.summary = 'MediQuo SDK for iOS'
    spec.authors = { "MediQuo, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2021 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:engineeringmediquo/mediquo-sdk-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.mediquo.com'
    spec.ios.deployment_target = '12.3'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.2'

    spec.module_name = 'MediQuo_Base'
    spec.frameworks = ['Foundation', 'CoreLocation', 'UIKit']

    spec.static_framework = true

    spec.dependency 'Alamofire', '4.9.1'
    spec.dependency 'CryptoSwift', '1.4.0'
    spec.dependency 'Device', '3.2.1'
    spec.dependency 'MessageKit', '3.3.0'
    spec.dependency 'PromiseKit', '6.12.0'
    spec.dependency 'SDWebImage', '5.3.1'
    spec.dependency 'SnapKit', '5.0.0'
    spec.dependency 'Socket.IO-Client-Swift', '15.2.0'
    spec.dependency 'SwiftGen', '6.4.0'
    spec.dependency 'SwiftFormat/CLI', '0.40.14'
    spec.dependency 'SwiftLint', '0.37.0'

    spec.dependency 'mediquo-videocall-lib', '0.0.54'

    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    #   Only for development
    spec.source_files = 'MediQuo-Base/**/*.{swift}'
    spec.resource_bundles = {
        'MediQuo_Base' => ['MediQuo-Base/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
    }
    spec.resources = ['MediQuo-Base/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
end
