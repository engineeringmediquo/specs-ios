Pod::Spec.new do |spec|
    spec.name = 'MediQuo'
    spec.version = "0.36.14"
    spec.summary = 'MediQuo Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2019 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'MediQuo'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit', 'Photos', 'StoreKit']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Schema', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Controller', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Remote', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Socket', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Storage', "#{spec.version}"
    spec.ios.dependency 'AlamofireImage', '3.6.0'
    spec.ios.dependency 'MessageKit', '3.0.0'
    spec.ios.dependency 'R.swift', '5.1.0'
    spec.ios.dependency 'Swinject', '2.7.1'
    spec.ios.dependency 'SwinjectStoryboard', '~>2.2.1'
    spec.ios.dependency 'RxCocoa', '5.0.1'
    spec.ios.dependency 'RxDataSources', '4.0.1'
    spec.ios.dependency 'SDWebImage', '5.3.1'
    spec.ios.dependency 'Device', '3.2.1'
    spec.ios.dependency 'SwiftyMarkdown', '1.0.0'
    spec.ios.dependency 'SwipeCellKit', '2.7.1'

    spec.source_files = 'MediQuo/MediQuo/Source/**/*.{swift}'
    spec.resources = ['MediQuo/MediQuo/**/*.{der,lproj,storyboard,xib,xcassets,strings,json,otf}', 'MediQuo/MediQuo/Resource/Environment-Debug.plist', 'MediQuo/MediQuo/Resource/Environment-Release.plist']
end
