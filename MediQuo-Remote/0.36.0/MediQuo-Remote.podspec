Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Remote'
    spec.version = "0.36.0"
    spec.summary = 'MediQuo Remote Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2019 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'MediQuoRemote'
    spec.ios.frameworks = ['Foundation', 'UIKit']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Schema', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Controller', "#{spec.version}"
    spec.ios.dependency 'AlamofireImage', '3.6.0'
    spec.ios.dependency 'Alamofire', '4.9.1'

    spec.source_files = 'Remote/Remote/Source/**/*.{swift}'
end
