Pod::Spec.new do |spec|
    spec.name = 'mediquo-professionals-lib'
    spec.version = '2.0.101'
    spec.summary = 'MediQuo Professional Framework for iOS Lib'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2019 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-professionals-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'mediquo_professionals_lib'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit']

    spec.ios.dependency 'Alamofire', '4.9.1'
    spec.ios.dependency 'CryptoSwift', '1.1.3'
    spec.ios.dependency 'Device', '3.2.1'
    spec.ios.dependency 'LicensePlist', '2.9.0'
    spec.ios.dependency 'MessageKit', '3.0.0'
    spec.ios.dependency 'PromiseKit', '6.12'
    spec.ios.dependency 'SDWebImage', '5.3.1'
    spec.ios.dependency 'Socket.IO-Client-Swift', '15.2.0'
    spec.ios.dependency 'SwiftGen', '6.1.0'
    spec.ios.dependency 'SwiftFormat/CLI', '0.40.14'
    spec.ios.dependency 'SwiftLint', '0.37.0'
    spec.ios.dependency 'Swinject', '2.7.1'
    spec.ios.dependency 'SwinjectStoryboard', '2.2.1'
    spec.ios.dependency 'SwiftDate', '6.1.1'
    spec.ios.dependency 'CountryPickerSwift', '1.8.2'
    spec.ios.dependency 'SwipeCellKit', '2.7.1'
    spec.ios.dependency 'AWSS3', '2.12.1'
    spec.ios.dependency 'Toast-Swift', '5.0.1'
    spec.ios.dependency 'eMMa', '4.5.2'

    #   Only for development
   spec.source_files = 'mediquo-professionals-lib/**/*.{swift}'
   spec.resource_bundles = {
       'mediquo_professionals_lib' => ['mediquo-professionals-lib/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
    }
   spec.resources = ['mediquo-professionals-lib/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
end
