Pod::Spec.new do |spec|
    spec.name = 'MediQuoWp'
    spec.version = '0.11.0'
    spec.summary = 'MediQuo Wordpress Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2018 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/worpress-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'Mediquo_wp'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit']

    spec.ios.dependency 'AlamofireImage', '3.6.0'
    spec.ios.dependency 'Alamofire', '4.9.1'
    spec.ios.dependency 'MediQuo-Remote', '~>0.36.0'
    spec.ios.dependency 'MediQuo-Storage', '~>0.36.0'
    spec.ios.dependency 'R.swift', '5.1.0'

   spec.source_files = 'Mediquo-wp/Mediquo-wp/Source/**/*.{swift}'
   spec.resource_bundles = {
       'Mediquo-wp' => ['Mediquo-wp/Mediquo-wp/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets}']
   }
   spec.resources = ['Mediquo-wp/Mediquo-wp/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets}']
end
