#!/usr/bin/ruby

Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Schema'
    spec.version = '0.10'
    spec.summary = 'MediQuo Schema Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    # spec.source = { :git => 'https://gitlab.com/mediquo/medi-ios.git', :tag => "#{spec.version}" }
    spec.source = { :http => "https://mediquo.bintray.com/generic/MediQuoSchema_0.10.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoSchema'
    spec.ios.frameworks = ['Foundation', 'UIKit']
    spec.ios.vendored_frameworks = 'MediQuo/MediQuoSchema.framework'

    spec.ios.dependency 'MediQuo-Core', "0.10"

    # spec.source_files = 'Schema/Schema/Source/**/*.{swift}'
end
