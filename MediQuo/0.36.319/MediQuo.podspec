Pod::Spec.new do |spec|
    spec.name = 'MediQuo'
    spec.version = "0.36.319"
    spec.summary = 'MediQuo Framework for iOS'
    spec.authors = { "MediQuo, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2021 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:engineeringmediquo/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.mediquo.com'
    spec.ios.deployment_target = '12.3'
    spec.platform = :ios, '12.3'
    spec.swift_version = '5.2'

    spec.module_name = 'MediQuo'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit', 'Photos', 'StoreKit']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Schema', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Controller', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Remote', "#{spec.version}"
    spec.ios.dependency 'MediQuo-Storage', "#{spec.version}"
    spec.ios.dependency 'AlamofireImage', '3.6.0'
    spec.ios.dependency 'MessageKit', '3.3.0'
    spec.ios.dependency 'R.swift', '5.1.0'
    spec.ios.dependency 'Swinject', '2.7.1'
    spec.ios.dependency 'SwinjectStoryboard', '~>2.2.1'
    spec.ios.dependency 'RxCocoa', '5.0.1'
    spec.ios.dependency 'RxDataSources', '4.0.1'
    spec.ios.dependency 'SDWebImage', '5.3.1'
    spec.ios.dependency 'Device', '3.2.1'
    spec.ios.dependency 'SwiftyMarkdown', '1.0.0'
    spec.ios.dependency 'SwipeCellKit', '2.7.1'
    spec.ios.dependency 'Socket.IO-Client-Swift', '15.2.0'
    spec.ios.dependency 'SnapKit', '5.0.0'
    spec.ios.dependency 'lottie-ios', '3.2.1'

    spec.source_files = 'MediQuo/MediQuo/Source/**/*.{swift}'
    spec.resources = ['MediQuo/MediQuo/**/*.{der,lproj,storyboard,xib,xcassets,strings,json,otf}', 'MediQuo/MediQuo/Resource/Environment-Debug.plist', 'MediQuo/MediQuo/Resource/Environment-Release.plist']
end
