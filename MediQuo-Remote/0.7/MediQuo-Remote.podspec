Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Remote'
    spec.version = '0.7'
    spec.summary = 'MediQuo Remote Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    spec.source = { :http => 'https://mediquo.bintray.com/generic/Remote_0.7.zip' }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoRemote'
    # spec.preserve_paths = 'MediQuo/Remote.framework'
    # spec.source_files = 'Remote.framework/Headers/*.{h}'
    # spec.ios.public_header_files = 'Remote.framework/Headers/*.h'
    spec.ios.vendored_frameworks = 'MediQuo/Remote.framework'
    spec.ios.frameworks = ['Foundation', 'UIKit']
    spec.ios.dependency 'Alamofire', '~> 4.5.1'
    spec.ios.dependency 'RxSwift', '~> 4.0.0'
end
