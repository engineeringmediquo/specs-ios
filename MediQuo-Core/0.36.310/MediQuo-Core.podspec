Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Core'
    spec.version = "0.36.310"
    spec.summary = 'MediQuo Core Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2019 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '12.3'
    spec.platform = :ios, '12.3'
    spec.swift_version = '5.2'

    spec.module_name = 'MediQuoCore'
    spec.ios.frameworks = ['Foundation', 'UIKit', 'CoreLocation', 'AVFoundation']

    spec.source_files = 'Core/Core/Source/**/*.{swift}'
    spec.resource_bundles = {
       'MediQuo' => ['Core/Core/Resource/**/*.{stencil}']
    }
end
