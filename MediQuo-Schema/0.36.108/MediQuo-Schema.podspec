Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Schema'
    spec.version = "0.36.108"
    spec.summary = 'MediQuo Schema Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2019 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-lib-ios.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'MediQuoSchema'
    spec.ios.frameworks = ['Foundation', 'UIKit', 'AdSupport']

    spec.ios.dependency 'MediQuo-Core', "#{spec.version}"

    spec.source_files = 'Schema/Schema/Source/**/*.{swift}'
end
