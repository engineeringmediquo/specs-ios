Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Base'
    spec.version = '0.0.2'
    spec.summary = 'MediQuo Base for iOS'
    spec.authors = { "Medipremium, S.L." => "engineering@mediquo.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2020 Medipremium S.L. All rights reserved.' }
    spec.source = { :git => "git@bitbucket.org:dllort-medipremium/mediquo-base.git", :tag => spec.version }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '11.0'
    spec.platform = :ios, '11.0'
    spec.swift_version = '5.0'

    spec.module_name = 'MediQuo_Base'
    spec.frameworks = ['Foundation', 'CoreLocation', 'UIKit']

    spec.static_framework = true

    spec.dependency 'Alamofire', '5.2'
    spec.dependency 'CryptoSwift', '1.3.1'
    spec.dependency 'Device', '3.2.1'
    spec.dependency 'Socket.IO-Client-Swift', '15.2.0'
    spec.dependency 'SwiftGen', '6.2.1'
    spec.dependency 'SwiftFormat/CLI', '0.44.17'
    spec.dependency 'SwiftLint', '0.39.2'

    #   Only for development
    spec.source_files = 'MediQuo-Base/**/*.{swift}'
    spec.resource_bundles = {
        'MediQuo_Base' => ['MediQuo-Base/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
    }
    spec.resources = ['MediQuo-Base/Resource/**/*.{der,plist,lproj,storyboard,xib,xcassets,strings,json,otf}']
end
